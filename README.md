# Duplicate Record Detector

## Overview

A callable function for finding duplicate records. User specifies object name, fields to check for duplicates, and desired return fields for the object. The function returns a Map of grouped duplicates for further processing.

## Front-end Demo (~30 sec)

[This video shows a simple LWC that calls the function and shows the grouped results](https://www.youtube.com/watch?v=dllQPDkQShI). In this example, we run a search on Lead for matching LastName/Email/City.

## A Screenshot

### Front-end Showing Result

![Front-end Showing Result](media/front-end.png)