public with sharing class Tester {
    public static void testerFunc() {
        System.debug('START testerFunc');
        // Map<String, List<sObject>> x = findDuplicatesAndDetails('Lead', new List<String>{'Name', 'Email', 'City'}, new List<String>{'Name', 'Company', 'City'});
        System.debug('END testerFunc');
    }

    @AuraEnabled(cacheable=true)
    public static Map<String, List<sObject>> findDuplicatesAndDetails(String objectName, List<String> matchingFields, List<String> returnedFields) {
        System.debug('START findDuplicatesAndDetails');
        Map<String, List<sObject>> result = new Map<String, List<sObject>>();

        // Find combinations with duplicates
        List<AggregateResult> duplicateAggs = findDuplicateRecords(objectName, matchingFields);
        // Get duplicate record details
        if(duplicateAggs.size() > 0) {
            List<sObject> duplicateDetails = getDuplicateRecordDetails(objectName, duplicateAggs, matchingFields, returnedFields);
            // Group records with their duplicates
            result = groupDuplicates(duplicateDetails, matchingFields);

        }

        System.debug('END findDuplicatesAndDetails');
        return result;
    }

    private static List<AggregateResult> findDuplicateRecords(String objectName, List<String> matchingFields) {
        System.debug('START findDuplicateRecords');
        List<AggregateResult> result = new List<AggregateResult>();

        if(objectName != null && matchingFields.size() > 0) {
            String query = 'SELECT Count(Id) idCount,' + String.join(matchingFields, ',') + ' FROM ';
            query += objectName + ' GROUP BY ' + String.join(matchingFields, ',');
            query += ' HAVING COUNT(Id) > 1 ORDER BY COUNT(Id) DESC';
            System.debug('query: ');
            System.debug(query);

            result = Database.query(query);
            // for(AggregateResult eachAgg : result) {
            //     System.debug(eachAgg);
            // }
            // System.debug(result);
        }

        System.debug('END findDuplicateRecords');
        return result;
    }

    private static List<sObject> getDuplicateRecordDetails(String objectName, List<AggregateResult> aggs, List<String> matchingFields, List<String> returnedFields) {
        System.debug('START findDuplicateRecordDetails');
        List<sObject> result = new List<SObject>();

        // Get fields we will return in query
        List<String> combinedFields = new List<String>(matchingFields);
        combinedFields.addAll(returnedFields);
        List<String> gatheredFields = new List<String>(new Set<String>(combinedFields));

        // Build each Where Clause
        List<String> whereClauses = new List<String>();
        for(AggregateResult eachAgg : aggs) {
            String newClause = '(';
            for(String eachField : matchingFields) {
                newClause += eachField + ' = \'' + (String)eachAgg.get(eachField) + '\' AND ';
            }
            newClause = newClause.removeEnd(' AND ');
            whereClauses.add(newClause + ')');
        }

        // Put together complete query
        String query = 'SELECT ' + String.join(gatheredFields, ',') + ' FROM ' + objectName + ' WHERE ';
        for(String eachClause : whereClauses) {
            query += eachClause + ' OR ';
        }
        query = query.removeEnd(' OR ');
        System.debug('QUERY: ');
        System.debug(query);

        // Run query
        result = Database.query(query);
        // for(sObject eachObj : result) {
        //     System.debug(eachObj);
        // }
        
        System.debug('END findDuplicateRecordDetails');
        return result;
    }

    private static Map<String, List<sObject>> groupDuplicates(List<sObject> duplicateDetails, List<String> matchingFields) {
        System.debug('START findDuplicatesAndDetails');
        Map<String, List<sObject>> result = new Map<String, List<sObject>>();

        for(sObject eachRecord : duplicateDetails) {
            // Build composite key of matchingFields... ex: ['Ab', 'C', 'D'] ==> key='AbCD'
            String key = '';
            for(String eachField : matchingFields) {
                key += (String)eachRecord.get(eachField);
            }
            // System.debug('key: ' + key);
            // Assign value to key
            if(result.get(key) == null) {
                result.put(key, new List<sObject>{eachRecord});
            }
            else {
                List<sObject> val = result.get(key);
                val.add(eachRecord);
                result.put(key, val);
            }
        }

        // for(String k : result.keySet()) {
        //     System.debug('k: ' + k);
        //     System.debug(result.get(k));
        // }

        System.debug('END findDuplicatesAndDetails');
        return result;
    }

}
