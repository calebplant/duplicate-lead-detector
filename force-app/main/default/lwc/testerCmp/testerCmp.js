import { LightningElement, wire } from 'lwc';
import getDuplicates from '@salesforce/apex/Tester.findDuplicatesAndDetails'

export default class TesterCmp extends LightningElement {

    objectName = 'Lead';
    matchingFields = ['Name', 'Email', 'City'];
    returnedFields = ['Id', 'Name', 'Company', 'City', 'Email'];
    columns;
    data;
    selected;

    @wire(getDuplicates, {objectName: '$objectName', matchingFields: '$matchingFields', returnedFields: '$returnedFields'})
    fetchDuplicates(res) {
        if(res.data) {
            console.log('Response from server: ');
            console.log(res.data);
            this.columns = this.returnedFields.map(eachField => ({label: eachField, fieldName: eachField}));
            console.log('col: ');
            console.log(JSON.parse(JSON.stringify(this.columns)));
            this.data = Object.values(res.data).map((eachGroup, index) => {
                return {
                    records: eachGroup,
                    groupLabel: `Group ${index + 1} (${eachGroup.length})`
                }
            })
        }
        if(res.error) {
            console.error(error);
        }
    }

    handleToggleSection(event) {
        console.log('testerCmp :: handleToggleSelection');
    }

}